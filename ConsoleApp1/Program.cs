﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => RegisterPersonAsync());
            Task.Run(() => ListPeopleAsync());

            Console.Read();
        }

        private static async Task<List<PersonResponse>> ListPeopleAsync()
        {
            Console.WriteLine("*************** [POST] ***************");

            HttpClient client = new HttpClient();
            var people = new List<PersonResponse>();
            var urlBase = "http://localhost:81/";
            client.BaseAddress = new Uri(urlBase);
            var url = string.Concat(urlBase, "api/People/Get");

            var response = client.GetAsync(url).Result;
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                people = JsonConvert.DeserializeObject<List<PersonResponse>>(result);
            }

            foreach (var item in people)
            {
                Console.WriteLine(item.FullName);
            }

            return people;
        }

        private static async Task RegisterPersonAsync()
        {
            Console.WriteLine("*************** [GET] ***************");

            HttpClient client = new HttpClient();
            var urlBase = "http://localhost:81/";
            client.BaseAddress = new Uri(urlBase);
            var url = string.Concat(urlBase, "api/People/Insert");

            var model = new PersonRequest
            {
                FirstName = "JAMUTAQ",
                LastName = "ORTEGA"
            };

            //lo que se envia
            var request = JsonConvert.SerializeObject(model);            
            //como se envia
            var content = new StringContent(request, Encoding.UTF8, "application/json");
           
            var response = client.PostAsync(url, content).Result;
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine("Persona creada");
                var result = await response.Content.ReadAsStringAsync();
                var person = JsonConvert.DeserializeObject<PersonResponse>(result);
                Console.WriteLine(person.FullName);
            }
            else
            {
                Console.WriteLine("Error Del Servicio");
            }
        }
    }
}
